library(DESeq2)
library(GenomicRanges)

cnts <- read.csv("all_mRNA_nonzero_raw_counts_trim.csv",header=T,as.is=T,row.names=1)
# R is fucking idiotic
# it will not load in this CSV as anything other than strings
rnames <- rownames(cnts)
cnts <- data.frame(lapply(cnts,as.integer))
rownames(cnts) <- rnames

covs <- read.csv('/restricted/project/mlpd/universal_covariates.csv',stringsAsFactors=F)

covs <- DataFrame(covs[ match(colnames(cnts), covs$BU_ID) ,])
covs$Type <- factor(ifelse(substr(covs$Type,1,1)=="C","C","ND"))

d <- DataFrame(condition=covs$Type,
               Death=cut(covs$Death,c(0,45,60,75,90,120)),
               BU_ID=covs$BU_ID
              )

se <- SummarizedExperiment(as.matrix(cnts), colData=d)
colnames(se) <- colData(se)$BU_ID
names(assays(se)) <- c('counts')

dds <- DESeqDataSet(se,~ Death + condition)
colData(dds)$condition <- relevel(colData(dds)$condition, "C")

vsd <- varianceStabilizingTransformation(dds)
vst.cnts <- assay(vsd)
rownames(vst.cnts) <- rownames(cnts)
write.csv(vst.cnts,'all_DESeq2_norm_vst_counts_trim.csv')
