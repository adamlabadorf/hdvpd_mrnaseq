import gzip
import os
import sys
import re
from glob import glob
import pandas as pd

count_fns = glob('/restricted/projectnb/ml*d/mRNA/alignment/STAR/v21/*/*ORMAN.gencodev21.counts.txt')

samples = [re.search('ml.d_([HCP]_[^.]*).sorted.*',os.path.basename(fn)).group(1) for fn in count_fns]

count_m = pd.read_table(count_fns[0],index_col=0,names=('gene','count'))

counts = pd.DataFrame(index=count_m.index,columns=samples)
counts[samples[0]] = count_m

for sample,fn in zip(samples,count_fns) :
  print sample, fn
  counts[sample] = pd.read_table(fn,index_col=0,names=('gene','count'))

# strip out the Ensembl version number
counts.index = [re.sub('\.[^.]*$','',_) for _ in counts.index]

# filter out biotypes we aren't interested in

ensmapfn = '/restricted/projectnb/mlhd/annot/ENS_to_SYM_20140527_mart_export.txt.gz'
ensmap = pd.read_table(gzip.open(ensmapfn),index_col=0)
ensmap.columns
#ensmap['Gene Biotype'].unique()
#array(['protein_coding', 'miRNA', 'pseudogene', 'misc_RNA', 'lincRNA',
#       'snRNA', 'snoRNA', 'antisense', 'rRNA', 'processed_transcript',
#       'sense_intronic', 'sense_overlapping', 'IG_V_gene', 'IG_D_gene',
#       '3prime_overlapping_ncrna', 'IG_V_pseudogene',
#       'polymorphic_pseudogene', 'Mt_tRNA', 'Mt_rRNA', 'TR_V_pseudogene',
#       'IG_C_pseudogene', 'TR_C_gene', 'TR_J_gene', 'TR_V_gene',
#       'TR_J_pseudogene', 'IG_J_pseudogene', 'IG_J_gene', 'IG_C_gene',
#       'TR_D_gene', 'processed_pseudogene', 'LRG_gene'], dtype=object)
biotypes = ['protein_coding','lincRNA','processed_transcript','sense_intronic',
            'sense_overlapping',
            'IG_V_gene','IG_D_gene','IG_J_gene','IG_C_gene',
            'TR_V_gene','TR_D_gene','TR_J_gene','TR_C_gene']
poly_A_genes = ensmap.apply(lambda x: x['Gene Biotype'] in biotypes,axis=1)
poly_A_genes = ensmap.loc[poly_A_genes]

poly_A_count_genes = counts.index.intersection(poly_A_genes.index).unique()

poly_A_counts = counts.loc[poly_A_count_genes]

nonzero_genes = poly_A_counts.sum(axis=1)!=0

nonzero_counts = poly_A_counts[nonzero_genes]
nonzero_counts.to_csv('all_mRNA_nonzero_raw_counts.csv',index_label='ENSG')

