conda install -c bioconda -c r gcc jupyter pandas matplotlib networkx ruby \
  requests scipy statsmodels bioconductor-gseabase nodejs xlsxwriter snakemake \
  bedtools pysam
conda list --export > conda_packages.txt
