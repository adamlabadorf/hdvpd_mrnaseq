
ens.map <- read.table(gzfile("/restricted/projectnb/mlpd/annot/ENS_to_SYM_20150218_mart_export.txt.gz"),
                      sep="\t",
                      header=T,
                      as.is=T)
ens.map <- ens.map[!duplicated(ens.map$Ensembl.Gene.ID),]

biotypes <- c('protein_coding','lincRNA','processed_transcript','sense_intronic',
              'sense_overlapping',
              'IG_V_gene','IG_D_gene','IG_J_gene','IG_C_gene',
              'TR_V_gene','TR_D_gene','TR_J_gene','TR_C_gene')
ens.map <- ens.map[ens.map$Gene.type %in% biotypes,]
rownames(ens.map) <-ens.map$Ensembl.Gene.ID

hd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_HD_DE.txt",as.is=T)
pd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_PD_DE.txt",as.is=T)
nd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_ND_DE.txt",as.is=T)

output_top <- function(d) {
  d <- d[d$cnts.padj<0.01,]
  d <- d[order(d$cnts.padj,decreasing=F),]
  d$Symbol <- ens.map[d$ENSG,"Associated.Gene.Name"]
  d$biotype <- ens.map[d$ENSG,"Gene.type"]
  d$Symbol[is.na(d$Symbol)] <- d$ENSG[is.na(d$Symbol)]

  d$cnts.beta <- signif(d$cnts.beta,2)
  d$cnts.padj <- signif(d$cnts.padj,2)
  head(d[c('Symbol','cnts.beta','cnts.padj')],30)
}

write.csv(output_top(hd),"all_mRNA_nonzero_norm_counts_trim_firth_C_HD_DE_top100.txt",row.names=F,quote=F)
write.csv(output_top(pd),"all_mRNA_nonzero_norm_counts_trim_firth_C_PD_DE_top100.txt",row.names=F,quote=F)
write.csv(output_top(nd),"all_mRNA_nonzero_norm_counts_trim_firth_C_ND_DE_top100.txt",row.names=F,quote=F)
