This directory is used by the ENSG00000272403 extended analysis only. If you
would like to replicate this result, follow these steps.

Create aligned BAM and BAI files for each of the input samples from the GEO
accessions as appropriate, following this filename scheme:

`mlhd_{sample_name}.sorted.out.ORMAN.bam`
`mlhd_{sample_name}.sorted.out.ORMAN.bam.bai`

e.g.:

`mlpd_P_0026.sorted.out.ORMAN.bam`
`mlpd_P_0026.sorted.out.ORMAN.bam.bai`

The BAM files must be sorted, i.e. with `samtools sort <bam>`.

You can use `samtools index <bam>` to create the `.bai` file from a `.bam`
file.
