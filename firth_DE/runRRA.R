library(RobustRankAggreg)

ens.map <- read.table(gzfile("ENS_to_SYM_20150218_mart_export.txt.gz"),
                      sep="\t",
                      header=T,
                      as.is=T)
ens.map <- ens.map[!duplicated(ens.map$Ensembl.Gene.ID),]
rownames(ens.map) <-ens.map$Ensembl.Gene.ID

hd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_HD_DE_stdcoeff.txt",as.is=T)
pd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_PD_DE_stdcoeff.txt",as.is=T)
nd <- read.table("all_mRNA_nonzero_norm_counts_trim_firth_C_ND_DE_stdcoeff.txt",as.is=T)

# ND
all_nd <- list(HD=hd$ENSG[order(-hd$cnts.chisq)],
               PD=pd$ENSG[order(-pd$cnts.chisq)],
               ND=nd$ENSG[order(-nd$cnts.chisq)])
nd.rra <- aggregateRanks(all_nd)
nd.rra$Rank <- 1:length(nd.rra$Name)

# HDvPD
hd_pd <- list(HD=all_nd$HD,PD=all_nd$PD)
hd_pd.rra <- aggregateRanks(hd_pd)
hd_pd.rra$Rank <- 1:length(hd_pd.rra$Name)

# HDvND
hd_nd <- list(HD=all_nd$HD,ND=all_nd$ND)
hd_nd.rra <- aggregateRanks(hd_nd)
hd_nd.rra$Rank <- 1:length(hd_nd.rra$Name)

# PDvND
pd_nd <- list(PD=all_nd$PD,ND=all_nd$ND)
pd_nd.rra <- aggregateRanks(pd_nd)
pd_nd.rra$Rank <- 1:length(pd_nd.rra$Name)

# combine all the stuffs
all.rra <- cbind(nd.rra[nd$ENSG,c('Score','Rank')],
                 hd_pd.rra[nd$ENSG,c('Score','Rank')],
                 hd_nd.rra[nd$ENSG,c('Score','Rank')],
                 pd_nd.rra[nd$ENSG,c('Score','Rank')])

colnames(all.rra) <- c(paste("All",c("Score","Rank")),
                       paste("HDvPD",c("Score","Rank")),
                       paste("HDvND",c("Score","Rank")),
                       paste("PDvND",c("Score","Rank")))

# I want all the genes to be in the list, not just the sig ones
#sig.rra <- apply(all.rra[,c(1,3,5,7)],1,function(r) any(r<0.05))
#sig.all.rra <- all.rra[sig.rra, ]

sig.all.rra <- all.rra
sig.all.rra <- sig.all.rra[order(sig.all.rra[,2]),]

sig.all.rra <- data.frame(sig.all.rra)
sig.all.rra$Symbol <- ens.map[rownames(sig.all.rra),"Associated.Gene.Name"]

sig.all.rra$HD.cnts.beta <- hd[match(rownames(sig.all.rra),hd$ENSG),"cnts.beta"]
sig.all.rra$PD.cnts.beta <- pd[match(rownames(sig.all.rra),pd$ENSG),"cnts.beta"]
sig.all.rra$ND.cnts.beta <- nd[match(rownames(sig.all.rra),nd$ENSG),"cnts.beta"]

write.table(sig.all.rra,"HD_PD_ND_RRA_res.csv",sep=",")

# PLAN:
# Box 1: Abstract
# Box 2: Pipeline
# Box 3: DE stats
# Box 4-6: Heatmap of genes
# Box 7-8: Enrichment heatmap
