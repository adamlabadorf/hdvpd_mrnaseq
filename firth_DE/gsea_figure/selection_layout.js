// Generated by CoffeeScript 1.12.4
(function() {
  d3.selection.prototype.layout = function(direction, hpad, vpad) {
    var dx, dy;
    if (hpad == null) {
      hpad = 0;
    }
    if (vpad == null) {
      vpad = 0;
    }
    dx = 0;
    dy = 0;
    return this.each(function() {
      var el, el_height, el_width, ref, ref1, transf, x, y;
      el = d3.select(this);
      transf = d3.transform(el.attr("transform"));
      x = (ref = transf.translate.x) != null ? ref : 0;
      y = (ref1 = transf.translate.y) != null ? ref1 : 0;
      if (direction === "vertical" || direction === "both") {
        y = dy;
        el_height = d3.select(this).node().getBBox().height;
        dy += el_height + vpad;
      }
      if (direction === "horizontal" || direction === "both") {
        x = dx;
        el_width = d3.select(this).node().getBBox().width;
        dx += el_width + hpad;
      }
      return el.attr("transform", "translate(" + x + "," + y + ")");
    });
  };

}).call(this);
