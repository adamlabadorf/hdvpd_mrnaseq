// Generated by CoffeeScript 1.12.4
(function() {
  var ArrowPie, CatPie, Grid, InfoBox, Legend, TextRing, TrackPie, Venn, data, mouse_update, props, ring_g, setup, svg,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  mouse_update = svg = ring_g = props = null;

  data = gsea_data;

  setup = function() {
    var arrowpie, cat_group, color_scales, infobox, k;
    color_scales = {
      HD: d3.scale.threshold().domain([-.1, .1]).range(["#faa", "#fff", "#f00"]),
      PD: d3.scale.threshold().domain([-.1, .1]).range(["#afa", "#fff", "#0f0"]),
      ND: d3.scale.threshold().domain([-.1, .1]).range(["#aaf", "#fff", "#00f"])
    };
    props = {
      fig_width: 850,
      fig_height: 700,
      ring_width: 600,
      ring_height: 600,
      trans_time: 750,
      ease: "linear",
      track_arc_width: 25,
      color_scales: color_scales
    };
    d3.selection.prototype.moveToFront = function() {
      return this.each(function() {
        return this.parentNode.appendChild(this);
      });
    };
    d3.selection.prototype.moveToBack = function() {
      return this.each(function() {
        var firstChild;
        firstChild = this.parentNode.firstChild;
        if (firstChild) {
          return this.parentNode.insertBefore(this, firstChild);
        }
      });
    };
    $("#chart1").remove();
    if (typeof element === "undefined") {
      $("body").append("<svg id='chart1'></svg>");
    } else {
      element.append("<svg id='chart1'></svg>");
    }
    svg = d3.select("#chart1").attr("width", props.fig_width).attr("height", props.fig_height);
    ring_g = svg.append("g").attr("transform", "translate(" + props.fig_width / 2 + "," + props.fig_height / 2 + ")");
    cat_group = _.groupBy(data, function(d) {
      return d.Category;
    });
    _.chain(cat_group).keys().map(function(k, i) {
      var v;
      v = cat_group[k];
      return v.sort(function(a, b) {
        var cmpmap;
        cmpmap = function(a) {
          return [a.Category, (a.HD_NES !== 0) + (a.PD_NES !== 0) + (a.ND_NES !== 0), a.HD_NES !== 0, a.PD_NES !== 0, a.ND_NES !== 0, a.HD_NES > 0];
        };
        return d3.ascending(cmpmap(a), cmpmap(b));
      });
    });
    data = _.flatten((function() {
      var j, len, ref, results;
      ref = _.keys(cat_group).sort();
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        k = ref[j];
        results.push(cat_group[k]);
      }
      return results;
    })());
    props.figure_radius = Math.min(props.ring_width, props.ring_height) / 2;
    new Venn();
    new Legend();
    new CatPie();
    new TextRing();
    new TrackPie();
    arrowpie = new ArrowPie();
    infobox = new InfoBox();
    return mouse_update = function(d, flag) {
      arrowpie.update(d, flag);
      if (d.length === 1) {
        return infobox.update(d[0], flag);
      }
    };
  };

  CatPie = (function() {
    var arcTween;

    CatPie.prototype.paths = null;

    function CatPie() {
      this.update = bind(this.update, this);
      var arc_width, cat_arc, cat_count, cat_counts, cat_pie, cat_radius, d, g, j, len, nudges, pie, text_arc;
      cat_radius = props.figure_radius;
      cat_count = function() {
        var cat_cnt, cat_group;
        cat_group = _.groupBy(data, function(d) {
          return d.Category;
        });
        return cat_cnt = _.map(cat_group, function(d, k) {
          var r;
          return r = {
            cat: k,
            count: d.length
          };
        });
      };
      cat_counts = cat_count();
      pie = d3.layout.pie().sort(null).value(function(d) {
        return d.count;
      });
      cat_pie = function() {
        return pie(cat_counts);
      };
      arc_width = props.track_arc_width / 2;
      cat_arc = d3.svg.arc().outerRadius(cat_radius).innerRadius(cat_radius - arc_width * .95);
      text_arc = d3.svg.arc().outerRadius(cat_radius + 16).innerRadius(cat_radius + 16 - arc_width * .95);
      g = ring_g.append("g").attr({
        id: "cat_pie"
      });
      this.paths = g.selectAll("path.category").data(cat_pie).enter().append("path").attr({
        d: cat_arc,
        "class": "category"
      }).style({
        stroke: "#fff",
        fill: "#000"
      }).on("mouseover", function(cat) {
        return mouse_update(_.filter(data, function(d) {
          return d.Category === cat.data.cat;
        }), true);
      }).on("mouseout", function(cat) {
        return mouse_update(_.filter(data, function(d) {
          return d.Category === cat.data.cat;
        }), false);
      });
      nudges = {};
      for (j = 0, len = cat_counts.length; j < len; j++) {
        d = cat_counts[j];
        nudges[d.cat] = {
          x: 0,
          y: 0
        };
      }
      nudges.OT.x = -15;
      nudges["IM, NE"].x = 5;
      nudges["IM, NE"].y = 0;
      nudges["NE, TR"].x = -32;
      nudges["NE, TR"].y = 12;
      nudges["NE"].y = 5;
      nudges.DN.y = -7;
      nudges["IM, TR"].x = -35;
      nudges["IM, TR"].y = 10;
      nudges["TF, TR"].x = -25;
      nudges["TF, TR"].y = 0;
      nudges["AP"].y = 3;
      nudges["TF"].x = -10;
      nudges["IM"].x = -5;
      g.selectAll("text.category").data(cat_pie).enter().append("text").text(function(d) {
        return d.data.cat;
      }).attr({
        id: function(d) {
          return "label_" + d.data.cat.replace(" ", "_");
        },
        transform: function(d) {
          return "translate(" + text_arc.centroid(d) + ")";
        },
        x: function(d) {
          return nudges[d.data.cat].x;
        },
        y: function(d) {
          return nudges[d.data.cat].y;
        }
      }).style({
        color: "#fff",
        "font-size": "0.9em"
      });
    }

    arcTween = function(a) {
      var i;
      i = d3.interpolate(this._current, a);
      this._current = i(0);
      return function(t) {
        return arc(i(t));
      };
    };

    CatPie.prototype.update = function() {
      this.paths = this.paths.data(cat_pie);
      return this.paths.transition().ease(ease).duration(trans_time).attrTween("d", arcTween);
    };

    return CatPie;

  })();

  ArrowPie = (function() {
    function ArrowPie() {
      this.update = bind(this.update, this);
      var arrow_angle, arrow_arc, arrow_pie, g, track_radius;
      track_radius = props.figure_radius - props.track_arc_width / 2;
      arrow_arc = d3.svg.arc().outerRadius(track_radius).innerRadius(track_radius);
      arrow_pie = d3.layout.pie().sort(null).value(function(d) {
        return 1;
      });
      arrow_angle = function(d) {
        return (d.startAngle * 180 / Math.PI) % 360;
      };
      g = ring_g.append("g").attr({
        id: "arrow_pie"
      });
      ring_g.append("defs").append("g").attr({
        id: "triangle"
      }).append("path").attr({
        d: "M0 0 L6 0 L3 10 L0 0",
        fill: "black",
        transform: "translate(-3,0)"
      });
      g.selectAll("use").data(arrow_pie(data)).enter().append("use").style({
        opacity: 0
      }).attr({
        id: function(d) {
          return "gs_" + d.data.geneset;
        },
        "xlink:href": "#triangle",
        transform: function(d) {
          return "translate(" + (arrow_arc.centroid(d)) + ") rotate(" + (arrow_angle(d)) + ")";
        }
      }).on({
        click: function() {
          return d3.select(this).style("opacity", 1);
        }
      });
    }

    ArrowPie.prototype.update = function(d, flag) {
      return d.map(function(d) {
        var elem;
        elem = d3.selectAll("use#gs_" + d.geneset);
        return elem.style({
          opacity: flag ? 1 : 0
        });
      });
    };

    return ArrowPie;

  })();

  TrackPie = (function() {
    function TrackPie() {
      var hd_radius, nd_radius, pd_radius, track_radius;
      track_radius = props.figure_radius - props.track_arc_width;
      hd_radius = track_radius;
      this.track(hd_radius, props.color_scales.HD, "HD");
      pd_radius = hd_radius - props.track_arc_width;
      this.track(pd_radius, props.color_scales.PD, "PD");
      nd_radius = pd_radius - props.track_arc_width;
      this.track(nd_radius, props.color_scales.ND, "ND");
    }

    TrackPie.prototype.track = function(radius, color, field) {
      var arc, arc_width, arcs, g, paths, track_pie;
      arcs = {};
      arc_width = props.track_arc_width;
      arc = d3.svg.arc().outerRadius(radius).innerRadius(radius - arc_width * .95);
      arcs[field] = arc;
      g = ring_g.append("g").attr({
        id: "track_" + field
      });
      track_pie = d3.layout.pie().sort(null).value(function(d) {
        return 1;
      });
      paths = {};
      paths[field] = g.selectAll("path." + field).data(track_pie(data)).enter().append("path").attr({
        d: arc,
        "class": function(d, i) {
          return "HD gs_" + i;
        }
      }).style({
        stroke: "#fff",
        fill: function(d) {
          return color(d.data[field + "_NES"]);
        }
      }).on({
        "mouseover": function(d) {
          return mouse_update([d.data], true);
        },
        "mouseout": function(d) {
          return mouse_update([d.data], false);
        }
      }).each(function(d) {
        return this._current = d;
      });
      return paths[field].arc = arc;
    };

    return TrackPie;

  })();

  TextRing = (function() {
    function TextRing() {
      var arc_width, text_anch, text_angle, text_arc, text_groups, text_pie, text_radius, transf;
      text_groups = null;
      text_angle = function(d) {
        return (d.startAngle * 180 / Math.PI + 90) % 360;
      };
      transf = function(d) {
        var t_angle, tr;
        t_angle = text_angle(d);
        if (t_angle > 90 && t_angle < 270) {
          t_angle = (t_angle + 180) % 360;
        }
        tr = "rotate(" + t_angle + ")";
        return tr;
      };
      text_anch = function(d) {
        var t_angle, ta;
        t_angle = text_angle(d);
        ta = "start";
        if (t_angle > 90 && t_angle < 270) {
          ta = "end";
        }
        return ta;
      };
      text_pie = d3.layout.pie().sort(null).value(function(d) {
        return 1;
      });
      arc_width = props.track_arc_width;
      text_radius = props.figure_radius - 4 * props.track_arc_width;
      text_arc = d3.svg.arc().outerRadius(text_radius).innerRadius(text_radius);
      text_groups = ring_g.selectAll("text#gset").data(text_pie(data)).enter().append("text").attr({
        transform: function(d) {
          return "translate(" + (text_arc.centroid(d)) + ") " + (transf(d));
        },
        "text-anchor": text_anch,
        "alignment-baseline": "middle",
        "class": function(d, i) {
          return "gset gs_" + i;
        }
      }).style({
        "font-size": "3px"
      }).on({
        "mouseover": function(d) {
          return mouse_update([d.data], true);
        },
        "mouseout": function(d) {
          return mouse_update([d.data], false);
        }
      }).text(function(d) {
        return d.data.geneset;
      });
    }

    return TextRing;

  })();

  InfoBox = (function() {
    InfoBox.prototype.info_width = 300;

    InfoBox.prototype.info_height = 500;

    function InfoBox() {
      this.update = bind(this.update, this);
      var info_g, info_text_g, nes_boxes, nes_g, nes_text_space, nes_texts, nes_width;
      info_g = ring_g.append("g").attr({
        id: "infobox"
      }).style({
        opacity: 0
      });
      info_g.moveToFront();
      info_g.append("rect").attr({
        id: "inforect",
        width: this.info_width,
        height: this.info_height
      }).style({
        fill: "#fff",
        stroke: "#333",
        opacity: 0.95
      });
      info_text_g = info_g.append("g").attr({
        id: "infotext",
        transform: "translate(4,14)"
      });
      info_text_g.append("text").style({
        "font-color": "#aaa",
        "font-size": "10pt"
      }).text("Pathway:");
      info_text_g.append("text").attr({
        id: "geneset"
      }).style({
        color: "#000",
        "font-weight": "bold",
        "font-size": "12pt"
      }).text("");
      info_text_g.append("text").style({
        "color": "#aaa",
        "font-size": "10pt"
      }).text("Category:");
      info_text_g.append("text").attr({
        id: "category"
      }).style({
        color: "#000",
        "font-weight": "bold",
        "font-size": "12pt"
      }).text("");
      info_text_g.append("text").attr({
        id: "NES_label",
        dy: 10
      }).style({
        "font-color": "#333",
        "font-size": "11pt",
        "padding": "3px",
        "margin": "10px"
      }).text("Normalized Enrichment Score:");
      nes_g = info_text_g.append("g");
      nes_g.attr("id", "nes_boxes");
      nes_width = this.info_width / 3 - 3;
      nes_boxes = nes_g.selectAll("rect");
      nes_boxes.data(["HD", "PD", "ND"]).enter().append("rect").attr("id", function(d) {
        return "nes_box_" + d;
      }).attr("width", nes_width).attr("height", 20);
      nes_texts = nes_g.selectAll("text");
      nes_text_space = this.info_width / 3 / 2;
      nes_texts.data([["HD", nes_text_space], ["PD", nes_text_space * 3], ["ND", nes_text_space * 5]]).enter().append("text").style("text-anchor", "middle").style("font-size", "10pt").style("dominant-baseline", "central").style("color", "black").attr("transform", function(d) {
        return "translate(" + d[1] + " 10)";
      });
      nes_g.selectAll("rect").layout("horizontal");
      info_text_g.append("text").attr({
        id: "LE_label",
        dy: 10
      }).style({
        "font-color": "#333",
        "font-size": "11pt"
      }).text("Leading edge genes:");
      this.le_g = info_text_g.append("g").attr("id", "le_g");
    }

    InfoBox.prototype.update = function(gset, flag) {
      var coords, gene_lists, infobox_h, infobox_w, infobox_x, infobox_y, le;
      if (flag) {
        coords = d3.mouse(ring_g[0][0]);
        infobox_w = this.info_width;
        infobox_h = this.info_height;
        infobox_x = coords[0] < 0 ? props.ring_width / 2 - infobox_w : -props.ring_width / 2;
        infobox_y = coords[1] < 0 ? props.ring_height / 2 - infobox_h : -props.ring_height / 2;
        d3.select("#infobox").transition().duration(0).attr({
          transform: "translate(" + infobox_x + " " + infobox_y + ")"
        }).style({
          opacity: 0.95
        });
        d3.select("text#geneset").text(gset.geneset.split("_").join(" ")).wordwrap(infobox_w - 10);
        d3.select("text#category").text(gset.Category);
        d3.selectAll("g#nes_boxes > rect").attr("fill", function(d) {
          return props.color_scales[d]([gset[d + "_NES"]]);
        });
        d3.selectAll("g#nes_boxes > text").text(function(d) {
          var val;
          val = gset[d[0] + "_NES"].toFixed(2);
          if (val === "0.00") {
            return "";
          } else {
            return val;
          }
        });
        le = _.filter(_.sortBy(gset.leading_edge, function(d) {
          return d.id;
        }), function(v) {
          var ref;
          return v.genes.length > 0 && (ref = v.id, indexOf.call([0, 1, 2, 3, 4, 5, 6, 7, 8], ref) >= 0);
        });
        this.le_g.selectAll("*").remove();
        gene_lists = this.le_g.selectAll("g").data(le);
        gene_lists.enter().append("g").style({
          "dominant-baseline": "text-before-edge",
          "font-size": "1em"
        }).each(function(d) {
          d3.select(this).append("text").attr("id", function(d) {
            return "label_le_" + d.id;
          }).classed("label", true);
          return d3.select(this).append("text").attr("id", function(d) {
            return "text_le_" + d.id;
          }).classed("genes", true).style({
            "font-size": "0.7em"
          });
        });
        gene_lists.selectAll("g > text.label").text(function(d) {
          return d.name;
        }).style({
          "font-color": "#aaa",
          "font-size": "10pt"
        });
        gene_lists.each(function() {
          return d3.select(this).selectAll("g > text.genes").text(function(d) {
            return d.genes.sort().join(" ");
          }).wordwrap(infobox_w - 10);
        });
        gene_lists.each(function() {
          return d3.select(this).selectAll("g, text").layout("vertical", 0, 5);
        });
        gene_lists.sort(function(d) {
          return d.id;
        }).layout("vertical", 0, 5);
        d3.selectAll("g#infotext > *").layout("vertical", 0, 5);
        return d3.select("rect#inforect").attr("height", function() {
          return d3.select("g#infotext").node().getBBox().height + 10;
        });
      } else {
        return d3.select("#infobox").attr({
          transform: "translate(" + props.ring_width + " " + props.ring_height + ")"
        }).style({
          opacity: 0
        }).transition();
      }
    };

    return InfoBox;

  })();

  Venn = (function() {
    function Venn() {
      var common_g, common_gsets, common_num, hd_g, hd_gsets, hd_lab, hd_num, nd_g, nd_gsets, nd_lab, nd_num, pd_g, pd_gsets, pd_lab, pd_num, venn_common, venn_g, venn_hd, venn_nd, venn_pd;
      venn_g = svg.append("g");
      venn_g.attr("transform", "translate(730,10) scale(1.2)");
      hd_gsets = _.filter(data, function(d) {
        return (d.PD_NES === 0) && (d.HD_NES !== 0);
      });
      hd_g = venn_g.append("g");
      hd_g.on("mouseover", function() {
        d3.select("#hd_venn").attr("stroke-width", "2pt");
        return mouse_update(hd_gsets, true);
      });
      hd_g.on("mouseout", function() {
        d3.select("#hd_venn").attr("stroke-width", "0pt");
        return mouse_update(hd_gsets, false);
      });
      venn_hd = hd_g.append("path");
      venn_hd.attr("id", "hd_venn").attr("d", "m 0,0 c -6.0424,0.057 -12.0861,1.01211 -17.925,2.87188 -13.3459,4.2509 -24.826,12.97391 -32.4984,24.69219 -7.6724,11.71827 -11.0766,25.73024 -9.6359,39.6625 1.4407,13.93227 7.6389,26.95124 17.5468,36.85156 8.3905,8.38399 19.0488,14.13959 30.661,16.55782 11.6122,2.41823 23.6822,1.39548 34.7219,-2.94219 -12.5015,-1.60434 -24.1291,-7.29636 -33.0641,-16.18594 -8.9351,-8.88958 -14.6866,-20.48817 -16.3547,-32.98125 -1.6681,-12.49309 0.8384,-25.19321 7.1281,-36.11563 6.2897,-10.92242 16.0168,-19.46566 27.6594,-24.29375 2.3388,-0.96988 4.7394,-1.78413 7.186,-2.43594 2.4465,-0.65181 4.934,-1.14022 7.4453,-1.4625 -7.333,-2.88123 -15.1015,-4.29206 -22.8704,-4.21875 z").attr("fill", "#f00").attr("stroke", "black").attr("stroke-width", "0pt").style("opacity", 0.5);
      hd_num = hd_g.append("text").text("81").attr("x", -50).attr("y", 67).style({
        "font-size": "0.75em"
      });
      hd_lab = hd_g.append("text").text("HD").attr("x", -77).attr("y", 30);
      pd_gsets = _.filter(data, function(d) {
        return (d.PD_NES !== 0) && (d.HD_NES === 0);
      });
      pd_g = venn_g.append("g");
      pd_g.on("mouseover", function() {
        d3.select("#pd_venn").attr("stroke-width", "2pt");
        return mouse_update(pd_gsets, true);
      });
      pd_g.on("mouseout", function() {
        d3.select("#pd_venn").attr("stroke-width", "0pt");
        return mouse_update(pd_gsets, false);
      });
      venn_pd = pd_g.append("path");
      venn_pd.attr("id", "pd_venn").attr("d", "m 29.5,3.8 c -2.2466,0.021 -4.4969,0.17437 -6.7421,0.4625 2.8015,1.10075 5.5165,2.40739 8.125,3.90938 2.6084,1.50198 5.1025,3.19526 7.4609,5.06562 8.3928,6.65616 14.8842,15.40881 18.8188,25.37188 3.9345,9.96306 5.1755,20.78817 3.5953,31.38281 -1.5803,10.59463 -5.9263,20.58905 -12.5969,28.97031 -6.6707,8.38127 -15.4333,14.8577 -25.4031,18.77501 8.4,1.07799 16.9345,0.27782 24.9875,-2.34375 8.0529,-2.62158 15.4241,-6.99917 21.5797,-12.81562 8.7749,-8.29154 14.704,-19.15204 16.9328,-31.0172 2.2288,-11.86515 0.6445,-24.13782 -4.5235,-35.04844 -5.1679,-10.91061 -13.6591,-19.90935 -24.2515,-25.70156 -8.6064,-4.70617 -18.2474,-7.10213 -27.9829,-7.01094 z").attr("fill", "#0f0").attr("stroke", "black").attr("stroke-width", "0pt").style("opacity", 0.5);
      pd_num = pd_g.append("text").text("64").attr("x", 66).attr("y", 67).style({
        "font-size": "0.75em"
      });
      pd_lab = pd_g.append("text").text("PD").attr("x", 80).attr("y", 30);
      nd_gsets = _.filter(data, function(d) {
        return (d.ND_NES !== 0) && (d.HD_NES === 0) && (d.PD_NES === 0);
      });
      nd_g = venn_g.append("g");
      nd_g.on("mouseover", function() {
        d3.select("#nd_venn").attr("stroke-width", "2pt");
        return mouse_update(nd_gsets, true);
      });
      nd_g.on("mouseout", function() {
        d3.select("#nd_venn").attr("stroke-width", "0pt");
        return mouse_update(nd_gsets, false);
      });
      venn_nd = nd_g.append("path");
      venn_nd.attr("id", "nd_venn").attr("d", "m -42,103.8 c 5.69926,9.88455 13.93512,18.07087 23.85387,23.7104 9.91876,5.63952 21.16544,8.53043 32.57426,8.37305 11.40882,-0.15737 22.57148,-3.35739 32.33091,-9.26836 9.75943,-5.91097 17.76635,-14.32135 23.1908,-24.35934 -6.1556,5.81646 -13.52676,10.19398 -21.5797,12.81556 -8.05294,2.62157 -16.58848,3.42235 -24.98851,2.34436 -11.03971,4.33767 -23.10906,5.36038 -34.72124,2.94215 -11.61219,-2.41823 -22.26995,-8.17384 -30.66039,-16.55782").attr("fill", "#00f").attr("stroke", "black").attr("stroke-width", "0pt").style("opacity", 0.5);
      nd_num = nd_g.append("text").text("24").attr("x", 17).attr("y", 130).style({
        "font-size": "0.75em"
      });
      nd_lab = nd_g.append("text").text("ND").attr("x", 17).attr("y", 147);
      common_gsets = _.filter(data, function(d) {
        return (d.HD_NES !== 0) && (d.PD_NES !== 0) && (d.ND_NES !== 0);
      });
      common_g = venn_g.append("g");
      common_g.on("mouseover", function() {
        d3.select("#common_venn").attr("stroke-width", "2pt");
        return mouse_update(common_gsets, true);
      });
      common_g.on("mouseout", function() {
        d3.select("#common_venn").attr("stroke-width", "0pt");
        return mouse_update(common_gsets, false);
      });
      venn_common = common_g.append("path");
      venn_common.attr("id", "common_venn").attr("d", "m 22.8,4.2 c -2.5113,0.32228 -4.9988,0.81069 -7.4453,1.4625 -2.4466,0.65181 -4.8472,1.46606 -7.186,2.43594 -11.6426,4.82809 -21.3697,13.37133 -27.6594,24.29375 -6.2897,10.92242 -8.7962,23.62254 -7.1281,36.11563 1.6681,12.49308 7.4196,24.09167 16.3547,32.98125 8.935,8.88958 20.5626,14.5816 33.0641,16.18594 9.9698,-3.91731 18.7324,-10.39374 25.4031,-18.77501 6.6706,-8.38126 11.0166,-18.37568 12.5969,-28.97031 1.5802,-10.59464 0.3392,-21.41975 -3.5953,-31.38281 -3.9346,-9.96307 -10.426,-18.71572 -18.8188,-25.37188 -2.3584,-1.87036 -4.8525,-3.56364 -7.4609,-5.06562 -2.6085,-1.50199 -5.3235,-2.80863 -8.125,-3.90938 z").attr("fill", "#808").attr("stroke", "black").attr("stroke-width", "0pt").style("opacity", 0.5);
      common_num = common_g.append("text").text("145").attr("x", 7).attr("y", 67).style({
        "font-size": "0.75em"
      });
      venn_g.moveToBack();
    }

    return Venn;

  })();

  Grid = (function() {
    var _;
    return svg.selectAll("path").data((function() {
      var j, ref, results;
      results = [];
      for (_ = j = 0, ref = fig_width; j <= ref; _ = j += 10) {
        results.push(_);
      }
      return results;
    })()).enter().append("path").attr("d", function(d) {
      return "M0," + d + " H" + fig_width + " M" + d + ",0 V" + fig_height;
    }).style("stroke", "#888");
  });

  Legend = (function() {
    function Legend() {
      var enrich_g, hd_g, legend_g, nd_g, pathway_labels, pd_g;
      legend_g = svg.append("g");
      legend_g.attr("id", "legend").attr("transform", "translate(0,16)");
      legend_g.moveToBack();
      legend_g.append("text").text("Pathways related to:").style({
        "font-weight": "bold",
        "font-size": "0.9em"
      });
      pathway_labels = ["IM - Immune Response/Inflammation", "TF - Transcription Factor Targets", "TR - Transcription/Translation", "DN - DNA Damage and Repair", "AP - Apoptosis", "NE - Neurons", "OT - Other/Uncategorized"];
      legend_g.selectAll("text.label").data(pathway_labels).enter().append("text").text(function(d) {
        return d;
      });
      legend_g.append("text").text("Enriched (down/up):").style({
        "font-weight": "bold"
      });
      enrich_g = legend_g.append("g");
      hd_g = enrich_g.append("g");
      hd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#faa"
      });
      hd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#f00"
      });
      hd_g.append("text").text("HD").style({
        "dominant-baseline": "central"
      }).attr({
        y: 10
      });
      hd_g.selectAll("*").layout("horizontal");
      pd_g = enrich_g.append("g");
      pd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#afa"
      });
      pd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#0f0"
      });
      pd_g.append("text").text("PD").style({
        "dominant-baseline": "central"
      }).attr({
        y: 10
      });
      pd_g.selectAll("*").layout("horizontal");
      nd_g = enrich_g.append("g");
      nd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#aaf"
      });
      nd_g.append("rect").attr({
        width: 20,
        height: 20,
        fill: "#00f"
      });
      nd_g.append("text").text("ND").style({
        "dominant-baseline": "central"
      }).attr({
        y: 10
      });
      nd_g.selectAll("*").layout("horizontal");
      enrich_g.selectAll("g").layout("vertical");
      legend_g.selectAll("g#legend > *").layout("vertical");
    }

    return Legend;

  })();

  $(document).ready(setup);

}).call(this);
