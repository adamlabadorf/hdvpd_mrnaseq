// Generated by CoffeeScript 1.12.4
(function() {
  var wordwrap;

  wordwrap = function(width) {
    return this.each(function() {
      var line, line_height, results, text, tspan, word, words;
      text = d3.select(this);
      words = text.text().split(/\s+/).reverse();
      line = [];
      line_height = text.node().getBBox().height;
      tspan = text.text(null).append("tspan");
      results = [];
      while (words.length > 0) {
        word = words.pop();
        line.push(word);
        tspan.text(line.join(" "));
        if (tspan.node().getComputedTextLength() >= width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          results.push(tspan = text.append("tspan").attr("dy", line_height + "px").attr("dx", -tspan.node().getComputedTextLength() + "px").text(word));
        } else {
          results.push(void 0);
        }
      }
      return results;
    });
  };

  d3.selection.prototype.wordwrap = wordwrap;

}).call(this);
