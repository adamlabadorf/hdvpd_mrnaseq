mouse_update = svg = ring_g = props = null

data = gsea_data

setup = ->

  # define the global color scales for the different datasets
  color_scales =
    HD: d3.scale.threshold().domain([-.1,.1]).range ["#faa","#fff","#f00"]
    PD: d3.scale.threshold().domain([-.1,.1]).range ["#afa","#fff","#0f0"]
    ND: d3.scale.threshold().domain([-.1,.1]).range ["#aaf","#fff","#00f"]

  props =
    fig_width: 850
    fig_height: 700

    ring_width: 600
    ring_height: 600

    trans_time: 750
    ease: "linear"

    track_arc_width: 25
    color_scales: color_scales

  # https://github.com/wbkd/d3-extended
  d3.selection::moveToFront = ->
    this.each ->
      this.parentNode.appendChild this

  d3.selection::moveToBack = ->
      this.each ->
          firstChild = this.parentNode.firstChild
          if (firstChild)
              this.parentNode.insertBefore(this, firstChild)

  $("#chart1").remove()
  # element is defined in jupyter notebooks, only overwrite if it is null
  if typeof element == "undefined"
    $("body").append "<svg id='chart1'></svg>"
  else
    element.append "<svg id='chart1'></svg>"

  svg = d3.select("#chart1")
    .attr "width", props.fig_width
    .attr "height", props.fig_height

  ring_g = svg
    .append "g"
    .attr "transform", "translate(" + props.fig_width / 2 + "," + props.fig_height / 2 + ")"

  # sort the data appropriately
  cat_group = _.groupBy(data, (d) -> d.Category)
  _.chain(cat_group)
   .keys()
   .map (k,i) ->
      v = cat_group[k]
      v.sort (a,b) ->
         cmpmap = (a) -> [
           a.Category,
           (a.HD_NES!=0)+(a.PD_NES!=0)+(a.ND_NES!=0),
           a.HD_NES!=0,
           a.PD_NES!=0,
           a.ND_NES!=0,
           a.HD_NES>0
         ]
         d3.ascending cmpmap(a), cmpmap(b)
  data = _.flatten((cat_group[k]) for k in _.keys(cat_group).sort())

  # start drawing the figure
  props.figure_radius = Math.min(props.ring_width,props.ring_height)/2

  new Venn()
  new Legend()
  new CatPie()
  new TextRing()
  new TrackPie()
  arrowpie = new ArrowPie()
  infobox = new InfoBox()

  mouse_update = (d,flag) ->
    arrowpie.update(d,flag)
    if d.length == 1
      infobox.update(d[0],flag)


class CatPie

  paths: null

  constructor: () ->

    cat_radius = props.figure_radius

    cat_count =  ->
        # category track, only count if display == true
        cat_group = _.groupBy data, (d) -> d.Category
        cat_cnt = _.map cat_group, (d,k) ->
          r =
              cat:k
              count: d.length

    cat_counts = cat_count()
    pie = d3.layout.pie()
      .sort null
      .value (d) -> d.count

    cat_pie = () ->
          pie(cat_counts)

    arc_width = props.track_arc_width/2
    cat_arc = d3.svg.arc()
      .outerRadius cat_radius
      .innerRadius cat_radius - arc_width*.95

    text_arc = d3.svg.arc()
      .outerRadius cat_radius+16
      .innerRadius cat_radius+16 - arc_width*.95

    g = ring_g.append "g"
      .attr
        id: "cat_pie"

    @paths = g.selectAll "path.category"
      .data cat_pie
      .enter()
      .append("path")
      .attr
        d : cat_arc
        class: "category"
      .style
        stroke : "#fff"
        fill : "#000"
      .on "mouseover", (cat) ->
         mouse_update _.filter(data,(d) -> d.Category == cat.data.cat), true
      .on "mouseout", (cat) ->
         mouse_update _.filter(data,(d) -> d.Category == cat.data.cat), false

    nudges = {}
    nudges[d.cat] = {x:0,y:0} for d in cat_counts

    nudges.OT.x = -15

    nudges["IM, NE"].x = 5
    nudges["IM, NE"].y = 0

    nudges["NE, TR"].x = -32
    nudges["NE, TR"].y = 12

    nudges["NE"].y = 5

    nudges.DN.y = -7

    nudges["IM, TR"].x = -35
    nudges["IM, TR"].y = 10

    nudges["TF, TR"].x = -25
    nudges["TF, TR"].y = 0

    nudges["AP"].y = 3

    nudges["TF"].x = -10

    nudges["IM"].x = -5

    g.selectAll("text.category")
      .data(cat_pie)
      .enter()
      .append("text")
      .text (d) -> d.data.cat
      .attr
        id: (d) -> "label_"+d.data.cat.replace(" ","_")
        transform: (d) -> "translate("+text_arc.centroid(d)+")"
        x: (d) -> nudges[d.data.cat].x
        y: (d) -> nudges[d.data.cat].y
      .style
        color: "#fff"
        "font-size": "0.9em"

  arcTween = (a) ->
    i = d3.interpolate(this._current, a)
    this._current = i(0)
    (t) ->
      arc(i(t))

  update: =>

      @paths = @paths.data(cat_pie)
      @paths
        .transition()
        .ease(ease)
        .duration(trans_time)
        .attrTween("d", arcTween)

class ArrowPie

  constructor: () ->

    track_radius = props.figure_radius - props.track_arc_width/2

    arrow_arc = d3.svg.arc()
        .outerRadius track_radius
        .innerRadius track_radius

    arrow_pie = d3.layout.pie()
        .sort null
        .value (d) -> 1

    arrow_angle = (d) -> (d.startAngle*180/Math.PI)%360

    g = ring_g.append("g")
      .attr
        id: "arrow_pie"

    ring_g.append "defs"
       .append "g"
       .attr
          id: "triangle"
       .append "path"
       .attr
          d: "M0 0 L6 0 L3 10 L0 0"
          fill: "black"
          transform: "translate(-3,0)"

    g.selectAll "use"
      .data(arrow_pie(data))
      .enter()
      .append "use"
      .style
         opacity: 0
      .attr
         id: (d) -> "gs_"+d.data.geneset
         "xlink:href": "#triangle"
         transform: (d) -> "translate(#{arrow_arc.centroid(d)}) rotate(#{arrow_angle(d)})"
      .on
        click: () -> d3.select(this).style("opacity",1)

  update: (d,flag) =>
    # do update
    d.map (d) ->
      elem = d3.selectAll "use#gs_#{d.geneset}"
      elem
        .style
          opacity: if flag then 1 else 0

class TrackPie

  constructor: () ->

    #HD
    track_radius = props.figure_radius - props.track_arc_width
    hd_radius = track_radius
    @track(hd_radius,props.color_scales.HD,"HD")

    #PD
    pd_radius = hd_radius - props.track_arc_width
    @track(pd_radius,props.color_scales.PD,"PD")

    #ND
    nd_radius = pd_radius - props.track_arc_width
    @track(nd_radius,props.color_scales.ND,"ND")

  track: (radius,color,field) ->

    arcs = {}
    arc_width = props.track_arc_width
    arc = d3.svg.arc()
      .outerRadius radius
      .innerRadius radius - arc_width*.95
    arcs[field] = arc
    g = ring_g.append("g")
         .attr
          id: "track_"+field
    track_pie = d3.layout.pie()
      .sort null
      .value (d) -> 1

    paths = {}
    paths[field] = g.selectAll("path.#{field}")
      .data(track_pie(data))
      .enter().append("path")
      .attr
        d : arc
        class : (d,i) -> "HD gs_#{i}"
      .style
        stroke : "#fff"
        fill : (d) -> color(d.data["#{field}_NES"])
      .on
        "mouseover": (d) -> mouse_update([d.data],true)
        "mouseout": (d) -> mouse_update([d.data],false)
      .each (d) -> this._current = d
    paths[field].arc = arc

class TextRing

  constructor: () ->

    text_groups = null

    text_angle = (d) -> (d.startAngle*180/Math.PI + 90)%360
    transf = (d) ->
      t_angle = text_angle(d)
      if t_angle > 90 and t_angle < 270
          t_angle = (t_angle+180)%360

      tr = "rotate(#{t_angle})"
      tr

    text_anch = (d) ->
      t_angle = text_angle(d)
      ta = "start"
      ta = "end" if t_angle > 90 and t_angle < 270
      ta

    text_pie = d3.layout.pie()
      .sort null
      .value (d) -> 1

      #gset names
      arc_width = props.track_arc_width
      text_radius = props.figure_radius - 4*props.track_arc_width
      text_arc = d3.svg.arc()
         .outerRadius text_radius
         .innerRadius text_radius

    text_groups = ring_g.selectAll("text#gset")
      .data(text_pie(data))
      .enter().append("text")
      .attr
          transform: (d) -> "translate(#{text_arc.centroid(d)}) #{transf(d)}"
          "text-anchor": text_anch
          "alignment-baseline": "middle"
          class: (d,i) -> "gset gs_"+i
      .style
          "font-size":"3px"
      .on
          "mouseover": (d) -> mouse_update([d.data],true)
          "mouseout": (d) -> mouse_update([d.data],false)
      .text (d) ->  d.data.geneset

class InfoBox

  info_width: 300
  info_height: 500

  constructor: () ->

    # overall infobox group
    info_g = ring_g.append "g"
        .attr
            id: "infobox"
        .style
            opacity: 0

    info_g.moveToFront()

    # background rect
    info_g.append "rect"
        .attr
            id: "inforect"
            width: @info_width
            height: @info_height
        .style
            fill: "#fff"
            stroke: "#333"
            opacity: 0.95

    # inner group for infobox text elements
    info_text_g = info_g.append "g"
        .attr
            id: "infotext"
            transform: "translate(4,14)"

    # pathway label
    info_text_g.append "text"
        .style
            "font-color": "#aaa"
            "font-size": "10pt"
        .text "Pathway:"

    # word wrapped pathway
    info_text_g.append "text"
        .attr
            id: "geneset"
        .style
            color: "#000"
            "font-weight": "bold"
            "font-size": "12pt"
        .text ""

    # category label
    info_text_g.append "text"
        .style
            "color": "#aaa"
            "font-size": "10pt"
        .text "Category:"

    # category
    info_text_g.append "text"
        .attr
            id: "category"
        .style
            color: "#000"
            "font-weight": "bold"
            "font-size": "12pt"
        .text ""

    # NES score label
    info_text_g.append "text"
        .attr
          id: "NES_label"
          dy: 10
        .style
            "font-color": "#333"
            "font-size": "11pt"
            "padding": "3px"
            "margin": "10px"
        .text "Normalized Enrichment Score:"

    # NES scores
    nes_g = info_text_g.append "g"
    nes_g.attr "id", "nes_boxes"
    nes_width = @info_width/3-3
    nes_boxes = nes_g.selectAll "rect"
    nes_boxes.data ["HD","PD","ND"]
             .enter()
             .append "rect"
             .attr "id", (d) -> "nes_box_#{d}"
             .attr "width", nes_width
             .attr "height", 20

    nes_texts = nes_g.selectAll "text"
    nes_text_space = @info_width/3/2
    nes_texts.data [["HD",nes_text_space],["PD",nes_text_space*3],["ND",nes_text_space*5]]
             .enter()
             .append "text"
             .style "text-anchor", "middle"
             .style "font-size", "10pt"
             .style "dominant-baseline", "central"
             .style "color", "black"
             .attr "transform", (d) -> "translate(#{d[1]} 10)"

    nes_g.selectAll "rect"
         .layout "horizontal"

    # NES score label
    info_text_g.append "text"
      .attr
        id: "LE_label"
        dy: 10
      .style
        "font-color": "#333"
        "font-size": "11pt"
      .text "Leading edge genes:"

    # group for leading edge gene lists
    @le_g = info_text_g.append "g"
      .attr "id", "le_g"

  update: (gset,flag) =>

    if flag
      coords = d3.mouse ring_g[0][0]

      infobox_w = @info_width
      infobox_h = @info_height

      infobox_x = if coords[0] < 0 then props.ring_width/2-infobox_w else -props.ring_width/2
      infobox_y = if coords[1] < 0 then props.ring_height/2-infobox_h else -props.ring_height/2

      d3.select "#infobox"
        .transition()
        .duration(0)
        .attr
          transform: "translate(#{infobox_x} #{infobox_y})"
        .style
          opacity: 0.95

      d3.select "text#geneset"
        .text gset.geneset.split("_").join(" ")
        .wordwrap infobox_w-10

      d3.select "text#category"
        .text gset.Category

      d3.selectAll "g#nes_boxes > rect"
        .attr "fill", (d) -> props.color_scales[d]([gset["#{d}_NES"]])

      d3.selectAll "g#nes_boxes > text"
        .text (d) ->
          val = gset["#{d[0]}_NES"].toFixed(2)
          if val == "0.00"
            ""
          else
            val

      # prepare the leading edge data for the d3 join
      le = _.filter(
        _.sortBy(gset.leading_edge,(d) -> d.id),
        (v) ->
          v.genes.length > 0 and v.id in [0..8]
      )

      # remove and re add all the leading edge stuffs
      @le_g.selectAll "*"
           .remove()

      gene_lists = @le_g.selectAll "g"
        .data le

      gene_lists.enter()
        .append "g"
        .style
          "dominant-baseline": "text-before-edge"
          "font-size": "1em"
        .each (d) ->
          d3.select @
            .append "text"
            .attr "id", (d) -> "label_le_#{d.id}"
            .classed "label", true
          d3.select @
            .append "text"
            .attr "id", (d) -> "text_le_#{d.id}"
            .classed "genes", true
            .style "font-size": "0.7em"

      # set the label texts
      gene_lists.selectAll "g > text.label"
        .text (d) -> d.name
        .style
          "font-color": "#aaa"
          "font-size": "10pt"


      # layout and word wrap the individual
      gene_lists
        .each ->
          d3.select @
            .selectAll "g > text.genes"
            .text (d) ->
              d.genes.sort().join(" ")
            .wordwrap infobox_w-10

      # vertical layout of the text elements within each group
      gene_lists
        .each ->
          d3.select @
            .selectAll "g, text"
            .layout "vertical", 0, 5

      # vertical layout of the leading edge gene groups
      gene_lists
        .sort (d) -> d.id
        .layout "vertical", 0, 5

      # vertical layout of all the text elements in the infobox
      d3.selectAll "g#infotext > *"
        .layout "vertical", 0, 5

      d3.select "rect#inforect"
        .attr "height", -> d3.select("g#infotext").node().getBBox().height+10
    else
      d3.select "#infobox"
        .attr
          transform: "translate(#{props.ring_width} #{props.ring_height})"
        .style
          opacity: 0
        .transition()

# I copied this from an inkscape figure
class Venn

  constructor: () ->

    venn_g = svg.append "g"
    venn_g.attr "transform", "translate(730,10) scale(1.2)"

    hd_gsets = _.filter(data, (d) -> (d.PD_NES == 0) and (d.HD_NES != 0))
    hd_g = venn_g.append "g"
    hd_g.on "mouseover", ->
      d3.select "#hd_venn"
        .attr "stroke-width", "2pt"
      mouse_update hd_gsets, true
    hd_g.on "mouseout", ->
      d3.select "#hd_venn"
        .attr "stroke-width", "0pt"
      mouse_update hd_gsets, false
    venn_hd = hd_g.append "path"
    venn_hd.attr "id", "hd_venn"
      .attr "d", "m 0,0 c -6.0424,0.057 -12.0861,1.01211 -17.925,2.87188 -13.3459,4.2509 -24.826,12.97391 -32.4984,24.69219 -7.6724,11.71827 -11.0766,25.73024 -9.6359,39.6625 1.4407,13.93227 7.6389,26.95124 17.5468,36.85156 8.3905,8.38399 19.0488,14.13959 30.661,16.55782 11.6122,2.41823 23.6822,1.39548 34.7219,-2.94219 -12.5015,-1.60434 -24.1291,-7.29636 -33.0641,-16.18594 -8.9351,-8.88958 -14.6866,-20.48817 -16.3547,-32.98125 -1.6681,-12.49309 0.8384,-25.19321 7.1281,-36.11563 6.2897,-10.92242 16.0168,-19.46566 27.6594,-24.29375 2.3388,-0.96988 4.7394,-1.78413 7.186,-2.43594 2.4465,-0.65181 4.934,-1.14022 7.4453,-1.4625 -7.333,-2.88123 -15.1015,-4.29206 -22.8704,-4.21875 z"
      .attr "fill", "#f00"
      .attr "stroke", "black"
      .attr "stroke-width", "0pt"
      .style "opacity", 0.5
    hd_num = hd_g.append "text"
      .text "81"
      .attr "x", -50
      .attr "y", 67
      .style
        "font-size": "0.75em"
    hd_lab = hd_g.append "text"
      .text "HD"
      .attr "x", -77
      .attr "y", 30

    pd_gsets = _.filter(data, (d) -> (d.PD_NES != 0) and (d.HD_NES == 0))
    pd_g = venn_g.append "g"
    pd_g.on "mouseover", ->
      d3.select "#pd_venn"
        .attr "stroke-width", "2pt"
      mouse_update pd_gsets, true
    pd_g.on "mouseout", ->
      d3.select "#pd_venn"
        .attr "stroke-width", "0pt"
      mouse_update pd_gsets, false
    venn_pd = pd_g.append "path"
    venn_pd.attr "id", "pd_venn"
      .attr "d", "m 29.5,3.8 c -2.2466,0.021 -4.4969,0.17437 -6.7421,0.4625 2.8015,1.10075 5.5165,2.40739 8.125,3.90938 2.6084,1.50198 5.1025,3.19526 7.4609,5.06562 8.3928,6.65616 14.8842,15.40881 18.8188,25.37188 3.9345,9.96306 5.1755,20.78817 3.5953,31.38281 -1.5803,10.59463 -5.9263,20.58905 -12.5969,28.97031 -6.6707,8.38127 -15.4333,14.8577 -25.4031,18.77501 8.4,1.07799 16.9345,0.27782 24.9875,-2.34375 8.0529,-2.62158 15.4241,-6.99917 21.5797,-12.81562 8.7749,-8.29154 14.704,-19.15204 16.9328,-31.0172 2.2288,-11.86515 0.6445,-24.13782 -4.5235,-35.04844 -5.1679,-10.91061 -13.6591,-19.90935 -24.2515,-25.70156 -8.6064,-4.70617 -18.2474,-7.10213 -27.9829,-7.01094 z"
      .attr "fill", "#0f0"
      .attr "stroke", "black"
      .attr "stroke-width", "0pt"
      .style "opacity", 0.5
    pd_num = pd_g.append "text"
      .text "64"
      .attr "x", 66
      .attr "y", 67
      .style
        "font-size": "0.75em"
    pd_lab = pd_g.append "text"
      .text "PD"
      .attr "x", 80
      .attr "y", 30

    nd_gsets = _.filter(data, (d) -> (d.ND_NES != 0) and (d.HD_NES == 0) and (d.PD_NES == 0))
    nd_g = venn_g.append "g"
    nd_g.on "mouseover", ->
      d3.select "#nd_venn"
        .attr "stroke-width", "2pt"
      mouse_update nd_gsets, true
    nd_g.on "mouseout", ->
      d3.select "#nd_venn"
        .attr "stroke-width", "0pt"
      mouse_update nd_gsets, false
    venn_nd = nd_g.append "path"
    venn_nd.attr "id", "nd_venn"
      .attr "d", "m -42,103.8 c 5.69926,9.88455 13.93512,18.07087 23.85387,23.7104 9.91876,5.63952 21.16544,8.53043 32.57426,8.37305 11.40882,-0.15737 22.57148,-3.35739 32.33091,-9.26836 9.75943,-5.91097 17.76635,-14.32135 23.1908,-24.35934 -6.1556,5.81646 -13.52676,10.19398 -21.5797,12.81556 -8.05294,2.62157 -16.58848,3.42235 -24.98851,2.34436 -11.03971,4.33767 -23.10906,5.36038 -34.72124,2.94215 -11.61219,-2.41823 -22.26995,-8.17384 -30.66039,-16.55782"
      .attr "fill", "#00f"
      .attr "stroke", "black"
      .attr "stroke-width", "0pt"
      .style "opacity", 0.5
    nd_num = nd_g.append "text"
      .text "24"
      .attr "x", 17
      .attr "y", 130
      .style
        "font-size": "0.75em"
    nd_lab = nd_g.append "text"
      .text "ND"
      .attr "x", 17
      .attr "y", 147

    common_gsets = _.filter(data, (d) -> (d.HD_NES != 0 ) and (d.PD_NES != 0) and (d.ND_NES != 0) )
    common_g = venn_g.append "g"
    common_g.on "mouseover", ->
      d3.select "#common_venn"
        .attr "stroke-width", "2pt"
      mouse_update common_gsets, true
    common_g.on "mouseout", ->
      d3.select "#common_venn"
        .attr "stroke-width", "0pt"
      mouse_update common_gsets, false
    venn_common = common_g.append "path"
    venn_common.attr "id", "common_venn"
      .attr "d", "m 22.8,4.2 c -2.5113,0.32228 -4.9988,0.81069 -7.4453,1.4625 -2.4466,0.65181 -4.8472,1.46606 -7.186,2.43594 -11.6426,4.82809 -21.3697,13.37133 -27.6594,24.29375 -6.2897,10.92242 -8.7962,23.62254 -7.1281,36.11563 1.6681,12.49308 7.4196,24.09167 16.3547,32.98125 8.935,8.88958 20.5626,14.5816 33.0641,16.18594 9.9698,-3.91731 18.7324,-10.39374 25.4031,-18.77501 6.6706,-8.38126 11.0166,-18.37568 12.5969,-28.97031 1.5802,-10.59464 0.3392,-21.41975 -3.5953,-31.38281 -3.9346,-9.96307 -10.426,-18.71572 -18.8188,-25.37188 -2.3584,-1.87036 -4.8525,-3.56364 -7.4609,-5.06562 -2.6085,-1.50199 -5.3235,-2.80863 -8.125,-3.90938 z"
      .attr "fill", "#808"
      .attr "stroke", "black"
      .attr "stroke-width", "0pt"
      .style "opacity", 0.5
    common_num = common_g.append "text"
      .text "145"
      .attr "x", 7
      .attr "y", 67
      .style
        "font-size": "0.75em"

    venn_g.moveToBack()

Grid = ( ->
  svg.selectAll "path"
     .data (_ for _ in [0..fig_width] by 10)
     .enter()
     .append "path"
     .attr "d", (d) -> "M0,"+d+" H"+fig_width+" M"+d+",0 V"+fig_height
     .style "stroke", "#888"
)

class Legend
  constructor: () ->
    legend_g = svg.append "g"
    legend_g.attr "id", "legend"
      .attr "transform", "translate(0,16)"
    legend_g.moveToBack()

    legend_g.append "text"
      .text "Pathways related to:"
      .style
        "font-weight": "bold"
        "font-size": "0.9em"

    pathway_labels = [
      "IM - Immune Response/Inflammation",
      "TF - Transcription Factor Targets",
      "TR - Transcription/Translation",
      "DN - DNA Damage and Repair",
      "AP - Apoptosis",
      "NE - Neurons",
      "OT - Other/Uncategorized"
    ]

    legend_g.selectAll "text.label"
      .data pathway_labels
      .enter()
      .append "text"
      .text (d) -> d

    legend_g.append "text"
      .text "Enriched (down/up):"
      .style
        "font-weight": "bold"

    enrich_g = legend_g.append "g"

    hd_g = enrich_g.append "g"
    hd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#faa"
    hd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#f00"
    hd_g.append "text"
      .text "HD"
      .style
        "dominant-baseline": "central"
      .attr
        y: 10
    hd_g.selectAll "*"
      .layout "horizontal"

    pd_g = enrich_g.append "g"
    pd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#afa"
    pd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#0f0"
    pd_g.append "text"
      .text "PD"
      .style
        "dominant-baseline": "central"
      .attr
        y: 10
    pd_g.selectAll "*"
      .layout "horizontal"

    nd_g = enrich_g.append "g"
    nd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#aaf"
    nd_g.append "rect"
      .attr
        width: 20
        height: 20
        fill: "#00f"
    nd_g.append "text"
      .text "ND"
      .style
        "dominant-baseline": "central"
      .attr
        y: 10
    nd_g.selectAll "*"
      .layout "horizontal"

    enrich_g.selectAll "g"
      .layout "vertical"

    legend_g.selectAll "g#legend > *"
      .layout "vertical"

$(document).ready setup
