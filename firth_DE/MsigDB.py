import csv
import numpy as np
import pandas as pd
import os
import re

from glob import glob
from collections import defaultdict, OrderedDict
from scipy.sparse import lil_matrix, csr_matrix
from scipy.stats import hypergeom
from statsmodels.sandbox.stats.multicomp import fdrcorrection0

class GeneSet(object) :
  def __init__(self,name,genes,url=None) :
    self.name = name
    self.genes = genes
    self.url = url

class PythonCollection(object) :
  def __init__(self,fn,name,system=None,version=None) :
    self.fn = fn
    self.name = name
    self.gsets = {}
    self._rlookup = defaultdict(set)
    for r in csv.reader(open(fn),delimiter='\t'):
      self.gsets[r[0]] = GeneSet(name=r[0], url=r[1], genes=r[2:])
      for g in r[2:]:
        self._rlookup[g].add(r[0])
  def get_genesets(self,genes=None) :
    if genes is None :
      return set(self.gsets.keys())
    else :
      return reduce(lambda a,b : a.union(b),(self._rlookup[g] for g in genes))
  def get_frequency(self,genes) :
    gset = set(genes)

class Collection(object) :
  def __init__(self,fn,name,system=None,version=None) :
    self.fn = fn
    self.name = name
    self.gsets = []
    self.genes = set()

    # get # of genes and genesets
    for r in csv.reader(open(fn),delimiter='\t') :
      self.gsets.append(r[0])
      self.genes.update(r[2:])

    gset_idx = pd.Series(range(len(self.gsets)),index=self.gsets)
    gene_idx = pd.Series(range(len(self.genes)),index=list(self.genes))
    self._gset_mat = lil_matrix((gset_idx.size,gene_idx.size),dtype='bool')

    for r in csv.reader(open(fn),delimiter='\t') :
      self._gset_mat[gset_idx[r[0]],gene_idx[r[2:]]] = 1
    self._gset_mat = self._gset_mat.tocsc()

    self._gset_idx = gset_idx
    self._gene_idx = gene_idx
    self._gset_counts = pd.Series(
      np.squeeze(self._gset_mat.toarray().sum(axis=1)),
      index=self.gsets
    )

  def get_counts(self,genes=None) :
    if genes is None :
      return self._gset_counts
    com = set(genes).intersection(self.genes)
    counts = np.squeeze(self._gset_mat[:,self._gene_idx[com]].toarray().sum(axis=1))
    return pd.Series(counts,index=self.gsets)

  def get_hyper(self,genes,N=None,mh='fdr') :
    gene_counts = self.get_counts(genes)
    # x is from gene_counts
    M = N if N else len(self.genes)
    # n is from self._gset_counts
    N = len(genes)

    hyper_mat = pd.concat([gene_counts,self._gset_counts],axis=1)
    hyper = hyper_mat.apply(
      lambda x,M,N : hypergeom.sf(x[0],M,x[1],N),
      args=(M,N,),
      axis=1
    )
    #return pd.Series(hyper,index=self.gsets)
    if isinstance(mh,str) :
      if mh.lower().startswith('bonf') :
        hyper *= len(self.gsets)
      if mh.lower().startswith('fdr') :
        hyper = fdrcorrection0(hyper)[1]

    return pd.Series(hyper,index=self.gsets)

  def get_gset(self,gset) :
    idx = np.squeeze(self._gset_mat[self._gset_idx[gset],:].toarray())
    return self._gene_idx[idx].index.tolist()

class MsigDB(object) :
  def __init__(self,path='.') :
    db_fns = sorted(glob(os.path.join(path,'*.gmt')))
    self.collections = OrderedDict()
    fn_patt = r'(c\d\.[^.]*)\.(v.*)\.([^.]*).gmt$'
    for fn in db_fns :
      name,version,system = re.search(fn_patt,fn).groups()
      self.collections[name] = Collection(fn,name,system=system,version=version)
    self.names = self.collections.keys()

  def __getitem__(self,item) :
    return self.collections[item]

  def __iter__(self) :
    for v in self.collections.values() :
      yield v

  def get(self,collections) :
    for c in collections :
      yield self.collections[c]
