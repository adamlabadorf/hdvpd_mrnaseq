FIGDIR=gsea_figure
wget http://d3js.org/d3.v3.min.js -O $FIGDIR/d3.v3.min.js
wget http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js -O $FIGDIR/underscore-min.js
wget https://code.jquery.com/jquery-3.1.1.min.js -O $FIGDIR/jquery.js
coffee -o $FIGDIR -c $FIGDIR/gsea_figure.coffee
coffee -o $FIGDIR -c .git_repos/55bqq/selection_layout.coffee
coffee -o $FIGDIR -c .git_repos/eezqx/wordwrap.coffee

zip -r $FIGDIR.zip $FIGDIR
