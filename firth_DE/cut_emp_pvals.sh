#!/bin/bash

PATTS="ND HD PD"
for patt in $PATTS
do
  echo $PATT
  for fn in `ls perms/perm_${patt}_a*.txt`
  do
      bio-table --unshift-headers --in-format split --strip-quotes --split-on ' ' --columns cnts.chisq $fn > ${fn}_cut
  done

  paste perms/*_cut > perms/perm_${patt}_mat.txt
  rm perms/*_cut
done
