# Source code for HD vs PD mRNA-Seq analysis

![[foo](http://creativecommons.org/licenses/by-sa/4.0/)](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Reference:

Labadorf, Adam, Seung H. Choi, and Richard H. Myers. 2017. “Evidence for a Pan-Neurodegenerative Disease Response in Huntington’s and Parkinson's Disease Expression Profiles.” Frontiers in Molecular Neuroscience 10: 430. [PubMed](https://pubmed.ncbi.nlm.nih.gov/29375298/)

The code in this repository produces the results, figures, and tables from the
manuscript starting from the normalized counts.


## Setup

These instructions were tested in a 64-bit Debian-based linux environment. If
you are using a different OS your mileage may vary.

First, clone this repository:

```bash
git clone https://bitbucket.org/adamlabadorf/hdvpd_mrnaseq.git
```

Anaconda was used to record the software requirements for running the code. To
set up the environment to run the software, run:

```bash
cd hdvpd_mrnaseq
conda create -n hdvpd_mrnaseq -c bioconda -c r --file conda_packages.txt
source activate hdvpd_mrnaseq
Rscript install_r_packages.R
```

## Running the analysis

All of these commands must be run from within the `firth_DE` directory.

The first step is running the Firth logistic regression on the normalized
counts:

```bash
./firth_DE.qsub ND # for ND analysis, repeat for HD and PD
# or, if using a cluster
qsub firth_DE/firth_DE.qsub ND # for ND analysis, repeat for HD and PD
```

**NB:** The R script uses mcapply and 16 cores. To generate the permutations
used in the significance determination:

```bash
./firth_DE_perm.qsub ND # for ND analysis, repeat for HD and PD
# or, if using a cluster
qsub firth_DE/firth_DE)_perm.qsub ND # for ND analysis, repeat for HD and PD
```

When the permutations have finished, combine the permutation p-values with:

```bash
./cut_emp_pvals.sh
```

The main DE result, along with versions of the manuscript figures and other
diagnostic plots, is computed in the jupyter notebook `Firth DE.ipynb`.

The scripts were submitted using the SGE cluster software, so you may have to
modify those scripts for your own environment.